class Shark:
    def __init__(self, name):
        self.name = name 
                

    def swim(self):
        print(self.name + " is swimming.")

    def be_awesome(self):
        print(self.name + " is being awesome.")
    
    ##def age(self):
      ##  print(self.name + " is " + self.age + "years old")


def main():
    sammy = Shark("Sammy")
    sammy.swim()
    stevie = Shark("Stevie")
    stevie.be_awesome()
    

if __name__ == "__main__":
    main()